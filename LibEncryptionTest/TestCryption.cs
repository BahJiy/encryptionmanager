using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using Xunit;
using LibEncryption;

namespace LibEncryptionTest {
	public class TestingCryption {
		/// Testing Xor \\
		// Basic testing
		[Theory]
		[InlineData ( 0x00, 0xff, 0xff )]
		[InlineData ( 0xff, 0xff, 0x00 )]
		[InlineData ( 0xab, 0x54, 0xff )]
		[InlineData ( 0xab, 0xba, 0x11 )]
		[InlineData ( 0xab, 0x00, 0xab )]
		[InlineData ( 0x77, 0x32, 0x45 )]
		public void TestXor ( byte value, byte amount, byte result ) => Assert.Equal ( result, Cryption.Xor ( value, amount ) );
		// Testing reflexive
		[Theory]
		[InlineData ( 0x00, 0xff )]
		[InlineData ( 0xff, 0xff )]
		[InlineData ( 0xab, 0x54 )]
		[InlineData ( 0xab, 0xba )]
		[InlineData ( 0xab, 0x00 )]
		[InlineData ( 0x77, 0x32 )]
		public void TestXorReflexive ( byte value, byte amount ) {
			byte result = Cryption.Xor ( value, amount );
			Assert.Equal ( value, Cryption.Xor ( result, amount ) );
			Assert.Equal ( amount, Cryption.Xor ( result, value ) );
		}

		/// Testing the rolling capabilities \\\
		// Test basic power of 2
		[Theory]
		[InlineData ( 0 )]
		[InlineData ( 1 )]
		[InlineData ( 8 )]
		[InlineData ( 9 )]
		[InlineData ( 10 )]
		[InlineData ( 16 )]
		[InlineData ( 17 )]
		public void TestRoll_Basic ( int value ) {
			Assert.Equal ( Math.Pow ( 2, value % 8 ), Cryption.LRoll ( 1, value ) );
			// 0x80 == 1000 0000
			Assert.Equal ( 0x80 / Math.Pow ( 2, value % 8 ), Cryption.RRoll ( 0x80, value ) );
		}
		// Test rolling of random bytes
		[Theory]
		[InlineData ( 0xff, 4, 0xff, 0xff )]
		[InlineData ( 0xff, 17, 0xff, 0xff )]
		[InlineData ( 0b10101011, 3, 0b01011101, 0b01110101 )]
		[InlineData ( 0b10101011, 7, 0b11010101, 0b01010111 )]
		[InlineData ( 0b10101011, 8, 0b10101011, 0b10101011 )]
		[InlineData ( 0b10101011, 10, 0b10101110, 0b11101010 )]
		[InlineData ( 0b10101011, 16, 0b10101011, 0b10101011 )]
		[InlineData ( 0b10101011, 17, 0b01010111, 0b11010101 )]
		public void TestRoll_Advance ( byte value, int offset, byte resultL, byte resultR ) {
			Assert.Equal ( resultL, Cryption.LRoll ( value, offset ) );
			Assert.Equal ( resultR, Cryption.RRoll ( value, offset ) );
		}
		// Test that a left roll by x and then a right roll by x will result in the original byte value
		[Theory]
		[InlineData ( 0xab, 3 )]
		[InlineData ( 0x91, 7 )]
		[InlineData ( 0x09, 8 )]
		[InlineData ( 0xc2, 15 )]
		[InlineData ( 0xf1, 16 )]
		[InlineData ( 0x2e, 17 )]
		public void TestRoll_Equals ( byte value, int offset ) {
			// Left then Right
			byte result = Cryption.LRoll ( value, offset );
			Assert.Equal ( value, Cryption.RRoll ( result, offset ) );
			// Right then Left
			result = Cryption.RRoll ( value, offset );
			Assert.Equal ( value, Cryption.LRoll ( result, offset ) );
		}

		/// Testing add and subtract \\\
		// Independent Add testing
		[Theory]
		[InlineData ( 0xab, 5, 0xb1 )]
		[InlineData ( 0xab, 2, 0xae )]
		[InlineData ( 0xab, 200, 0x74 )]
		[InlineData ( 0xab, 255, 0xab )]
		public void TestAdd ( byte value, byte amount, byte result ) => Assert.Equal ( result, Cryption.Add ( value, amount ) );
		// Independent Subtract testing
		[Theory]
		[InlineData ( 0x00, 10, 0xf5 )]
		[InlineData ( 0xab, 5, 0xa5 )]
		[InlineData ( 0xab, 2, 0xa8 )]
		[InlineData ( 0xab, 200, 0xe2 )]
		[InlineData ( 0xab, 255, 0xab )]
		public void TestSub ( byte value, byte amount, byte result ) => Assert.Equal ( result, Cryption.Subtract ( value, amount ) );
		// Doing an add then sub using the same amount should result in the original value
		[Theory]
		[InlineData ( 0x00, 10 )]
		[InlineData ( 0xef, 177 )]
		[InlineData ( 0x97, 100 )]
		[InlineData ( 0xff, 255 )]
		[InlineData ( 0xff, 1 )]
		[InlineData ( 0xab, 0 )]
		public void TestAddSub ( byte value, byte amount ) {
			byte result = Cryption.Add ( value, amount );
			Assert.Equal ( value, Cryption.Subtract ( result, amount ) );
		}
		// Doing a sub then add using the same amount should result in the original value
		[Theory]
		[InlineData ( 0x00, 10 )]
		[InlineData ( 0xef, 177 )]
		[InlineData ( 0x97, 100 )]
		[InlineData ( 0xff, 255 )]
		[InlineData ( 0xff, 1 )]
		[InlineData ( 0xab, 0 )]
		public void TestSubAdd ( byte value, byte amount ) {
			byte result = Cryption.Subtract ( value, amount );
			Assert.Equal ( value, Cryption.Add ( result, amount ) );
		}
	}
}
