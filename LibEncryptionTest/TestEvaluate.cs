using System;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Xunit;
using LibEncryption;

namespace LibEncryptionTest {
    public class TestingEvaluate {
        private readonly Encryption e;
        private readonly Random random;
        public string RandomString ( int length ) {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string ( Enumerable.Repeat ( chars, length )
              .Select ( s => s [ random.Next ( s.Length ) ] ).ToArray ( ) );
        }

        public byte [] RandomBytes ( int length ) {
            return Enumerable.Repeat ( 0, length )
            .Select ( i => ( byte )random.Next ( 0, 256 ) ).ToArray ( );
        }

        public TestingEvaluate ( ) {
            random = new Random ( );
            e = new Encryption ( RandomString ( random.Next ( 4, 16 ) ), RandomString ( random.Next ( 4, 16 ) ) );
        }

        [Theory]
        [InlineData ( new byte [] { 1, 2, 3, 4 } )]
        [InlineData ( new byte [] { 13, 2, 42, 98 } )]
        [InlineData ( new byte [] { 255, 255, 255, 255 } )]
        [InlineData ( new byte [] { 0, 0, 0, 0 } )]
        [InlineData ( new byte [] { 255, 0, 255, 0 } )]
        [InlineData ( new byte [] { 0, 0, 255, 255 } )]
        public void TestEnDe ( byte [] data ) {
            var encryptedData = e.Encrypt ( data );
            var decryptedData = e.Decrypt ( encryptedData );

            Assert.NotEqual ( encryptedData, decryptedData );
            Assert.Equal ( data, decryptedData );
        }

        [Theory]
        [InlineData ( 10 )]
        [InlineData ( 100 )]
        [InlineData ( 1000 )]
        [InlineData ( 10000 )]
        [InlineData ( 1 )]
        [InlineData ( 918 )]
        public void TestEnDeRandom ( int amount ) {
            var data = RandomBytes ( amount );
            TestEnDe ( data );
        }

        [Theory]
        [InlineData ( @"TestFiles/words2.txt" )]
        [InlineData ( @"TestFiles/pic.jpeg" )]
        public void TestFileEncryption ( string file ) {
            using ( Stream input = File.Open ( file, FileMode.Open ), outputE = File.Open ( file + ".en", FileMode.OpenOrCreate ) ) {
                e.Encrypt ( input, outputE );
            }
            using ( Stream outputE = File.Open ( file + ".en", FileMode.Open ), outputD = File.Open ( file + ".de", FileMode.OpenOrCreate ) ) {
                e.Decrypt ( outputE, outputD );
            }

            using ( Stream input = File.Open ( file, FileMode.Open ), outputD = File.Open ( file + ".de", FileMode.Open ) ) {
                Assert.Equal ( input.Length, outputD.Length );

                byte [] inputBytes, outputBytes;
                using ( BinaryReader inReader = new BinaryReader ( input ) ) {
                    using ( BinaryReader outReader = new BinaryReader ( outputD ) ) {
                        while ( inReader.BaseStream.Position < inReader.BaseStream.Length ||
                         outReader.BaseStream.Position < outReader.BaseStream.Length ) {
                            inputBytes = inReader.ReadBytes ( 1000 );
                            outputBytes = outReader.ReadBytes ( 1000 );
                            Assert.Equal ( inputBytes, outputBytes );
                        }
                    }
                }
            }
        }
    }
}
