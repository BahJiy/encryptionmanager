using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using Xunit;
using LibEncryption;

namespace LibEncryptionTest {
	public class TestHashing {
		private readonly Encryption e = new Encryption ( );
		private static Random random = new Random ( );

		// Provides a random string of character of requested length
		public static string RandomString ( int length ) {
			const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			return new string ( Enumerable.Repeat ( chars, length )
			  .Select ( s => s[ random.Next ( s.Length ) ] ).ToArray ( ) );
		}

		// Test that the data tuple of two keys with the same index should be the same
		[Theory]
		[InlineData ( 25, 0 )]
		[InlineData ( 10, 2 )]
		[InlineData ( 4, 9 )]
		[InlineData ( 1, 100 )]
		public void TestCryptSet ( int size, int index ) {
			string key = RandomString ( size );
			var data1 = e.CryptSet ( key, new List<byte> ( ), index );
			var data2 = e.CryptSet ( key, new List<byte> ( ), index );
			Assert.Equal ( data1, data2 );
		}

		// Test that we should NOT allow negative indexes
		[Theory]
		[InlineData ( -1 )]
		[InlineData ( -10 )]
		public void TestCryptSetException ( int index ) {
			string key = RandomString ( 25 );
			Assert.Throws<IndexOutOfRangeException> ( ( ) => e.CryptSet ( key, new List<byte> ( ), index ) );
		}

		// Test that the return tuple are of the correct elements (crypt = empty)
		[Theory]
		[InlineData ( "012345678", 0 )]
		[InlineData ( "012345678", 2 )]
		[InlineData ( "012345678", 7 )]
		public void TestCryptSetTuple ( string key, int index ) {
			var data = e.CryptSet ( key, new List<byte> ( ), index );
			Assert.Equal ( key[ index++ % key.Length ], data.Item1 );
			Assert.Equal ( key[ index++ % key.Length ], data.Item2 );
			Assert.Equal ( key[ index++ % key.Length ], data.Item3 );
			Assert.Equal ( key[ index++ % key.Length ], data.Item4 );
			Assert.Equal ( key[ index++ % key.Length ], data.Item5 );
			Assert.Equal ( key[ index++ % key.Length ], data.Item6 );
		}

		// Test that we can use the crypt list
		[Fact]
		public void TestCryptSetTupleCrypt_1 ( ) {
			string key = "012345678"; int index = 2;
			List<byte> crypt = Enumerable.Range ( 'A', 1 ).Select ( x => ( byte ) x ).ToList ( );
			var data = e.CryptSet ( key, crypt, index );
			Assert.Equal ( ( byte ) '2', data.Item1 );
			Assert.Equal ( ( byte ) '3', data.Item2 );
			Assert.Equal ( ( byte ) '4', data.Item3 );
			Assert.Equal ( ( byte ) '5', data.Item4 );
			Assert.Equal ( ( byte ) '6', data.Item5 );
			Assert.Equal ( ( byte ) '7', data.Item6 );
		}
		[Fact]
		public void TestCryptSetTupleCrypt_2 ( ) {
			string key = "012345678"; int index = 10;
			List<byte> crypt = Enumerable.Range ( 'A', 5 ).Select ( x => ( byte ) x ).ToList ( );
			var data = e.CryptSet ( key, crypt, index );
			Assert.Equal ( ( byte ) '1', data.Item1 );
			Assert.Equal ( ( byte ) 'C', data.Item2 );
			Assert.Equal ( ( byte ) 'D', data.Item3 );
			Assert.Equal ( ( byte ) 'E', data.Item4 );
			Assert.Equal ( ( byte ) '5', data.Item5 );
			Assert.Equal ( ( byte ) '6', data.Item6 );
		}
		[Fact]
		public void TestCryptSetTupleCrypt_3 ( ) {
			string key = "0123"; int index = 22;
			List<byte> crypt = Enumerable.Range ( 'A', 11 ).Select ( x => ( byte ) x ).ToList ( );
			var data = e.CryptSet ( key, crypt, index );
			Assert.Equal ( ( byte ) '2', data.Item1 );
			Assert.Equal ( ( byte ) 'I', data.Item2 );
			Assert.Equal ( ( byte ) 'J', data.Item3 );
			Assert.Equal ( ( byte ) 'K', data.Item4 );
			Assert.Equal ( ( byte ) '3', data.Item5 );
			Assert.Equal ( ( byte ) '0', data.Item6 );
		}

		// Insure that the hash data are allways the same whenever we give it the same key
		// Check that the data length are correct
		[Theory]
		[InlineData ( 4 )]
		[InlineData ( 10 )]
		[InlineData ( 15 )]
		[InlineData ( 16 )]
		public void TestMakeKey16 ( int size ) {
			string key = RandomString ( size );
			var data1 = e.MakeKey ( key );
			var data2 = e.MakeKey ( key );
			Assert.Equal ( 16, data1.Length );
			Assert.Equal ( data1, data2 );
		}
		[Theory]
		[InlineData ( 17 )]
		[InlineData ( 20 )]
		[InlineData ( 30 )]
		[InlineData ( 32 )]
		public void TestMakeKey32 ( int size ) {
			string key = RandomString ( size );
			var data1 = e.MakeKey ( key );
			var data2 = e.MakeKey ( key );
			Assert.Equal ( 32, data1.Length );
			Assert.Equal ( data1, data2 );
		}
		[Theory]
		[InlineData ( 33 )]
		[InlineData ( 50 )]
		[InlineData ( 71 )]
		[InlineData ( 128 )]
		public void TestMakeKey128 ( int size ) {
			string key = RandomString ( size );
			var data1 = e.MakeKey ( key );
			var data2 = e.MakeKey ( key );
			Assert.Equal ( 128, data1.Length );
			Assert.Equal ( data1, data2 );
		}

		// Some hash check to ensure the that MakeKey will return the same hash always
		[Fact]
		public void TestHashCheck_1 ( ) {
			string key = "Q21NTLVRFS";
			string hash = "99253192328117217248174708149162171102253";
			var data = e.MakeKey ( key );
			Assert.Equal ( hash, string.Join ( "", data ) );
		}
		[Fact]
		public void TestHashCheck_2 ( ) {
			string key = "Y27FXKIBDP1VCEAUGWI6";
			string hash = "912349226520661606422322833180233556485467738955887368986987762331388";
			var data = e.MakeKey ( key );
			Assert.Equal ( hash, string.Join ( "", data ) );
		}
		[Fact]
		public void TestHashCheck_3 ( ) {
			string key = "JBF9";
			string hash = "16239165707470747074200150012908070";
			var data = e.MakeKey ( key );
			Assert.Equal ( hash, string.Join ( "", data ) );
		}
		[Fact]
		public void TestHashCheck_4 ( ) {
			string key = "7HJ4GZ6R6DKSLHBMFRISJ4ZWM9E8JJ";
			string hash = "253243232140201643080672076720924501910854018612818513852243207230208174220230";
			var data = e.MakeKey ( key );
			Assert.Equal ( hash, string.Join ( "", data ) );
		}
		[Fact]
		public void TestHashCheck_5 ( ) {
			string key = "IWX7HPAO5NTJ";
			string hash = "1351892456111120613243617847397125203";
			var data = e.MakeKey ( key );
			Assert.Equal ( hash, string.Join ( "", data ) );
		}
		[Fact]
		public void TestHashCheck_6 ( ) {
			string key = "9AGZ140GONSQ2BIF";
			string hash = "240295187163219324072207171212895109148";
			var data = e.MakeKey ( key );
			Assert.Equal ( hash, string.Join ( "", data ) );
		}
		[Fact]
		public void TestHashCheck_7 ( ) {
			string key = "7N7Y2VBAFHX588T9E8WY625WPOT5EN43E4Y838CO9FUUG5HQ3TL4OTIE";
			string hash = "472201002413121402181751931283812413344149222223253039505919517712921594228196135631111501544210822414717454135612492007118810525013515094177355555066708856846987545380846952698951675785717251767973555550667088561685115822412714557481421544510246250166501006914383204884713020325110216322816815519422422340";
			var data = e.MakeKey ( key );
			Assert.Equal ( hash, string.Join ( "", data ) );
		}
		[Fact]
		public void TestHashCheck_8 ( ) {
			string key = "Y94SI8U5013WZCSWRGU7IX4X4N9WM1W5";
			string hash = "1291574696161401458843868141167155227152986919711788245131151462188209272980213";
			var data = e.MakeKey ( key );
			Assert.Equal ( hash, string.Join ( "", data ) );
		}
		[Fact]
		public void TestHashCheck_9 ( ) {
			string key = "SKIOB";
			string hash = "96123410579837366757915318220843286";
			var data = e.MakeKey ( key );
			Assert.Equal ( hash, string.Join ( "", data ) );
		}
		[Fact]
		public void TestHashCheck_10 ( ) {
			string key = "F61M40EJVFG6IZ0HLQUGDBVCAPUVUQSZXM3J3IMEDGINF8G3R6H4YH8DKZLPFSZFXRI66RODDPD63I6CJ0EXIYAPNZ8E679URCZXZJ7ASE1JECKB2JGPOC69Q2SHSKGG";
			string hash = "71431622532231930163724821341281448124221132481425223214519968143685616271602395044403979213244209161365235641983412812823697237192144240184962405454480156208501822251642110125117372351310710984111241160198913117332101641801682432291121501362558281196235194191871018613293921119021822999198109152125361561364555243160142";
			var data = e.MakeKey ( key );
			Assert.Equal ( hash, string.Join ( "", data ) );
		}

		// Use the word database to test the redundancy of the hash
		[Fact]
		public void TestHashValueString ( ) {
			HashSet<byte[ ]> set = new HashSet<byte[ ]> ( );
			List<string> wordList = new List<string> ( );
			using ( StreamReader file = new StreamReader ( "TestFiles/words.txt" ) ) {
				string word;
				while ( ( word = file.ReadLine ( ) ) != null ) {
					if ( !set.Add ( e.MakeKey ( word ) ) ) {
						wordList.Add ( word );
					}
				}
			}
			Assert.True ( wordList.Count == 0, "There are " + wordList.Count + "duplicate hashes" );
		}

		// Test the redundancy of hash in numbers
		[Fact]
		public void TestHashValueInt ( ) {
			HashSet<byte[ ]> set = new HashSet<byte[ ]> ( );
			List<int> intList = new List<int> ( );
			for ( int i = 0; i < 1_000_000; i++ ) {
				if ( !set.Add ( e.MakeKey ( i.ToString ( ) ) ) ) {
					intList.Add ( i );
				}
			}
			Assert.True ( intList.Count == 0, "There are " + intList.Count + "duplicate hashes" );
		}

		// Test the redundancy of hash in alpha numeric strings
		[Fact]
		public void TestHashValueAlphaNumeric ( ) {
			HashSet<byte[ ]> set = new HashSet<byte[ ]> ( );
			List<String> textList = new List<String> ( );
			for ( int i = 1; i < 1_000_001; i++ ) {
				String text = RandomString ( i % 100 + 4 );
				if ( !set.Add ( e.MakeKey ( text ) ) ) {
					textList.Add ( text );
				}
			}
			Assert.True ( textList.Count == 0, "There are " + textList.Count + "duplicate hashes" );
		}
	}
}
