﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
[assembly: InternalsVisibleTo ( "LibEncryptionTest" )]

namespace LibEncryption {

	/// <summary>
	/// Class to interact with the encryption process
	/// </summary>
	public sealed class Encryption {
		private const int MAXBUFFER = 1500;

		private delegate byte[ ] Execute ( byte[ ] data );
		private readonly Evaluate eval;

		/// <summary>
		/// Create an encryption instance
		/// </summary>
		/// <param name="key1">First Key. Length must be between 4 and 128</param>
		/// <param name="key2">Second Key. Length must between 4 and 128</param>
		/// <exception cref="ArgumentOutOfRangeException">If the either keys' length is not within the boundary</exception>
		public Encryption ( string key1, string key2 ) {
			if ( key1.Length > 128 || key2.Length > 128 )
				throw new ArgumentOutOfRangeException ( "Key too long" );
			if ( key1.Length < 4 || key2.Length < 4 )
				throw new ArgumentOutOfRangeException ( "Key too short" );

			eval = new Evaluate ( MakeKey ( key1 ), MakeKey ( key2 ) );
		}

		/// <summary>
		/// Empty instance of Encryption
		/// </summary>
		internal Encryption ( ) { }

		/// <summary>
		/// Hash the Key into an array of bytes
		/// </summary>
		/// <param name="key">A non-empty/non-null string of length 4 or longer</param>
		/// <returns>An array of bytes of size 16, 32, or 128</returns>
		internal byte[ ] MakeKey ( string key ) {
			/* Hash Algorithm :
			for byte i; i + 2 =
				roll(key[i] ^ key[i+1], key[i+2]) >> ((key[i+3] - key[i+4]) % 2 == 0 ? 1 : 2) + (key[1+5] & 128)
				-> In such that i + n will wrap back to the beginning if needed.
				-> In such that if key.length < i, we will use byte[i + n], where n = # of time pass key.length, then key[i]
			Size of byte = { 16, 32, 64 )
			 */
			byte[ ] crypt;
			if ( key.Length <= 16 )
				crypt = new byte[ 16 ];
			else if ( key.Length <= 32 )
				crypt = new byte[ 32 ];
			else
				crypt = new byte[ 128 ];
			for ( int i = 0; i < crypt.Length; i++ ) {
				var data = CryptSet ( key, crypt.ToList ( ), i * 2 );
				crypt[ i ] = Cryption.LRoll ( ( byte ) ( data.Item1 ^ data.Item2 ),
					data.Item3 >> ( ( data.Item4 - data.Item5 ) % 2 == 0 ? 1 : 2 ) + ( data.Item6 & 128 ) );
			}
			return crypt;
		}

		/// <summary>
		/// Using the key and the encryption array, get the next five byte value to use for encryption based on the index value.
		/// The first value is always from the key but the next 4 elements are either from the key or crypt array.
		/// If the initial index is within the key range, the rest of the elements will be from the key (wrap when needed)
		/// else we will use the crypt array as much as possible before switching back to the key index is greater than the
		/// crypt array'slength. Used to create the hash of the key.
		/// </summary>
		/// <param name="key">The encryption key string; must be of length 4 or longer </param>
		/// <param name="crypt">The encryption byte list; must not be null</param>
		/// <param name="index">Current positive index requested</param>
		/// <exception cref="IndexOutOfRangeException">If index is < 0</exception>
		/// <returns>A tuple contains the next 5 bytes to use for encryption</returns>
		internal (int, int, int, int, int, int) CryptSet ( string key, List<byte> crypt, int index ) {
			if ( index < 0 )
				throw new IndexOutOfRangeException ( );
			// Get an index that lies inside the key array
			List<int> resultList = new List<int> ( 6 ) { 0, 0, 0, 0, 0, 0 };
			bool cryptArr = false;
			// Get first element using the key
			resultList[ 0 ] = key[ index++ % key.Length ];
			// Decide if we are allow to use the crypt list or not
			if ( index - 1 >= key.Length ) {
				// if the crypt list is new, we should start at 0 else, get the mod
				index = crypt.Count != 0 ? ( index - key.Length ) % crypt.Count : 0;
				cryptArr = true;
			}
			for ( int i = 1; i < 6; i++, index++ ) {
				// Do we have to use the crypt array based on the initial index value?
				if ( cryptArr && index < crypt.Count )
					// if our index position is inside the crypt array
					resultList[ i ] = crypt[ index ];
				else
					// if we do not need to use the crypt array or index position > crypt.Length
					resultList[ i ] = ( byte ) key[ index % key.Length ];
			}

			return (resultList[ 0 ], resultList[ 1 ], resultList[ 2 ], resultList[ 3 ], resultList[ 4 ], resultList[ 5 ]);
		}

		/// <summary>
		/// Given a string, encrypt it using the keys provided and return the encrypted bytes
		/// </summary>
		/// <param name="text">The string to be encrypted</param>
		/// <returns>An array of bytes that may or may not be the same size as the original string</returns>
		public byte[ ] Encrypt ( string text ) => Encrypt ( Encoding.ASCII.GetBytes ( text ) );

		/// <summary>
		/// Given an array of bytes, encrypt it using the keys provided and return the encrypted bytes
		/// </summary>
		/// <param name="data">The bytes to be encrypted</param>
		/// <returns>An array of bytes that may or may not be the same size as the original array</returns>
		public byte[ ] Encrypt ( byte[ ] data ) => eval.EncryptArray ( data );

		/// <summary>
		/// Given a string, decrypt it using the keys provided and return the decrypted bytes
		/// </summary>
		/// <param name="text">The string to be decrypted</param>
		/// <returns>An array of bytes that may or may not be the same size as the original string</returns>
		public byte[ ] Decrypt ( string text ) => Decrypt ( Encoding.ASCII.GetBytes ( text ) );

		/// <summary>
		/// Given an array of bytes, decrypt it using the provided keys and return the decrypted bytes.
		/// Note that the decrypted bytes may or may not be the original; it depends on if the keys used where
		/// the same. The returned byte array may or may not be the same size as the given array.
		/// </summary>
		/// <param name="data">The encrypted bytes to be decrypted</param>
		/// <returns>An array of decrypted bytes based on the keys provided</returns>
		public byte[ ] Decrypt ( byte[ ] data ) => eval.DecryptArray ( data );

		/// <summary>
		/// Given a file, encrypt it using the keys provided and write the bytes to file
		/// </summary>
		/// <param name="input">The file to be encrypted</param>
		/// <param name="output">The file name to write out the bytes</param>
		public void Encrypt ( Stream input, Stream output, int BUFFER = MAXBUFFER ) => FileCryption ( input, output, BUFFER, eval.EncryptArray );

		/// <summary>
		/// Given an encrypted file, decrypt it using the provided keys and write the data to file
		/// Note that the decrypted bytes may or may not be the original; it depends on if the keys used where
		/// the same. The decrypted byte may or may not be the same size as the file
		/// </summary>
		/// <param name="input">The encrypted file to be decrypted</param>
		/// <param name="output">The file to write the bytes to</param>

		public void Decrypt ( Stream input, Stream output, int BUFFER = MAXBUFFER ) => FileCryption ( input, output, BUFFER, eval.DecryptArray );

		private void FileCryption ( Stream input, Stream output, int BUFFER, Execute execute ) {
			using ( BlockingCollection<byte[ ]> buffer = new BlockingCollection<byte[ ]> ( 10 ) ) {

				Task read = Task.Run ( ( ) => {
					using ( BinaryReader b = new BinaryReader ( input ) ) {
						try {
							while ( b.BaseStream.Position < b.BaseStream.Length ) {
								buffer.Add ( b.ReadBytes ( BUFFER ) );
							}
							buffer.CompleteAdding ( );
						} catch ( InvalidOperationException ) {
						} catch ( Exception e ) {
							buffer.CompleteAdding ( );
							throw e;
						}
					}
				} );

				Task write = Task.Run ( ( ) => {
					using ( BinaryWriter b = new BinaryWriter ( output ) ) {
						try {
							while ( !buffer.IsCompleted ) {
								b.Write ( execute ( buffer.Take ( ) ) );
							}
						} catch ( InvalidOperationException ) {
						} catch ( Exception e ) {
							buffer.CompleteAdding ( );
							throw e;
						}
					}
				} );

				Task.WaitAll ( write, read );
				if ( write.Status != TaskStatus.RanToCompletion )
					throw new Exception ( "Unable to write to file" );
				if ( read.Status != TaskStatus.RanToCompletion )
					throw new Exception ( "Unable to read to file" );
			}
		}
	}
	public static partial class Extension {
		// TODO
	}
}
