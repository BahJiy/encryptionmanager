using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.CompilerServices;
[assembly: InternalsVisibleTo ( "LibEncryptionTest" )]

namespace LibEncryption {

	/// <summary>
	/// Provides basic encryption functionality
	/// </summary>
	internal sealed class Cryption {

		/// <summary>
		/// Xor two bytes together
		/// </summary>
		/// <param name="x">Byte 1</param>
		/// <param name="y">Byte 2</param>
		/// <returns>The result of byte 1 xor with byte 2</returns>
		static internal byte Xor ( byte x, byte y ) => ( byte ) ( ( int ) x ^ ( int ) y );

		/// <summary>
		/// Roll the bits left by the requested value.
		/// </summary>
		/// <param name="value">The byte to roll</param>
		/// <param name="amount">The amount to roll; should be positive</param>
		/// <returns>A byte rolled to the left by the requested amount</returns>
		static internal byte LRoll ( byte value, int amount ) {
			amount = amount % 8; // get tha max roll amount
			return ( byte ) ( ( value << amount ) | ( value >> ( 8 - amount ) ) );
		}

		/// <summary>
		/// Roll the bits right by the requested value.
		/// </summary>
		/// <param name="value">The byte to roll</param>
		/// <param name="amount">The amount to roll; should be positive</param>
		/// <returns>A byte rolled to the right by the requested amount</returns>
		static internal byte RRoll ( byte value, int amount ) {
			amount = amount % 8; // get tha max roll amount
			return ( byte ) ( ( value >> amount ) | ( value << ( 8 - amount ) ) );
		}

		/// <summary>
		/// Add the two bytes together with an extra 1
		/// </summary>
		/// <param name="value">The original byte value</param>
		/// <param name="amount">The amount to add in</param>
		/// <returns>The two byte2 added together with an extra 1</returns>
		static internal byte Add ( byte value, byte amount ) => ( byte ) ( value + 1 + amount );

		/// <summary>
		/// Subtract two bytes together with an extra 1
		/// </summary>
		/// <param name="value">The original byte value</param>
		/// <param name="amount">The amount to subtract from</param>
		/// <returns>The two bytes subtracted together with an extra 1</returns>
		static internal byte Subtract ( byte value, byte amount ) => ( byte ) ( ( ( int ) value ^ 0x0100 ) - 1 - amount );
	}
	public static partial class Extension {

	}
}
