using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.CompilerServices;
[assembly: InternalsVisibleTo ( "LibEncryptionTest" )]

namespace LibEncryption {
	/// <summary>
	/// Evaluate the keys and data and encrypt/decrypt the data
	/// </summary>
	internal sealed class Evaluate {
		// Store the keys given
		private readonly byte[ ] key1, key2;
		internal Evaluate ( byte[ ] key1, byte[ ] key2 ) {
			this.key1 = key1; this.key2 = key2;
		}

		internal byte[ ] EncryptArray ( byte[ ] data ) {
			//List<byte> encrypt = new List<byte> ( data.Length );
			byte[ ] encrypt = new byte[ data.Length ];

			for ( int index = 0; index < data.Length; index++ ) {
				byte dataByte = Index ( data, index );
				encrypt[ index ] = Encrypt ( dataByte, index );
			}
			return encrypt;
		}
		internal byte[ ] DecryptArray ( byte[ ] data ) {
			//List<byte> decrypt = new List<byte> ( data.Length );
			byte[ ] decrypt = new byte[ data.Length ];

			for ( int index = 0; index < data.Length; index++ ) {
				byte dataByte = Index ( data, index );
				decrypt[ index ] = Decrypt ( dataByte, index );
			}
			return decrypt;
		}

		internal byte Encrypt ( byte oldByte, int index ) {
			byte newByte = oldByte;
			if ( Index ( key1, index ) % 0x03 <= Index ( key2, index ) % 0x03 )
				newByte = Cryption.Xor ( oldByte, Index ( key1, index ) );
			if ( Index ( key1, index ) % 0x07 >= Index ( key2, index ) % 0x07 )
				newByte = Cryption.Add (
						Cryption.Xor ( oldByte, Index ( key1, index ) ),
						Index ( key2, index ) );
			if ( Index ( key1, index ) % 0x0B <= Index ( key2, index ) % 0x0F ) {
				newByte = Cryption.Subtract (
						Cryption.Xor ( oldByte, Index ( key1, index ) ),
						Index ( key2, index )
				);
			}
			if ( Index ( key2, index ) <= Index ( key1, index ) )
				newByte = Index ( key2, index ) <= 0x0F ?
					Cryption.RRoll ( newByte, Index ( key1, index ) ) :
					Cryption.LRoll ( newByte, Index ( key1, index ) );

			return newByte;
		}

		internal byte Decrypt ( byte oldByte, int index ) {
			byte newByte = oldByte;

			if ( Index ( key2, index ) <= Index ( key1, index ) )
				newByte = oldByte = Index ( key2, index ) <= 0x0F ?
					Cryption.LRoll ( oldByte, Index ( key1, index ) ) :
					Cryption.RRoll ( oldByte, Index ( key1, index ) );
			if ( Index ( key1, index ) % 0x03 <= Index ( key2, index ) % 0x03 )
				newByte = Cryption.Xor ( oldByte, Index ( key1, index ) );
			if ( Index ( key1, index ) % 0x07 >= Index ( key2, index ) % 0x07 )
				newByte = Cryption.Xor (
					Cryption.Subtract ( oldByte, Index ( key2, index ) ),
					Index ( key1, index )
				);
			if ( Index ( key1, index ) % 0x0B <= Index ( key2, index ) % 0x0F ) {
				newByte = Cryption.Xor (
					Index ( key1, index ),
					Cryption.Add ( oldByte, Index ( key2, index ) )
				);
			}

			return newByte;
		}

		/// <summary>
		/// Safely index into array and return the byte at that location
		/// </summary>
		/// <param name=index>The index into array</param>
		private byte Index ( byte[ ] array, int index ) => array[ index % array.Length ];

		/// <summarY>
		/// Choose the nonzero byte and return it.!-- To be used with encrypt[index]/decrypt[index] and data array
		/// </summary>
		/// <para name=b1> The byte from the encrypt/decrypt array</para>
		/// <para name=b2> The byte[] to choose from if b1 is 0</para>
		/// <para name=index> The index into b2</para>
		private byte NonZero ( byte b1, byte[ ] b2, int index ) => b1 == 0 ? Index ( b2, index ) : b1;
	}
}
