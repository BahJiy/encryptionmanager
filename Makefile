run: LibEncryption/ ConsoleApp/
	TERM=xterm dotnet build

test: LibEncryption/ LibEncryptionTest/
	Term=xterm dotnet build
	cp -r LibEncryptionTest/TestFiles/ LibEncryptionTest/bin/Debug/netcoreapp2.0/
	TERM=xterm dotnet test LibEncryptionTest/LibEncryptionTest.csproj

clean: LibEncryption/ LibEncryptionTest/
	rm -rd LibEncryption/obj LibEncryption/bin || :
	rm -rd LibEncryptionTest/obj LibEncryptionTest/bin || :
	rm -rd ConsoleApp/obj ConsoleApp/bin/ || :
