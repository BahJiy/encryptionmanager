﻿using System;
using System.Text;
using LibEncryption;
using System.Linq;
using System.Collections.Generic;
using System.IO;

namespace ConsoleEncrypt {
    class Program {
        private struct Config {
            public bool encrypt;
            public bool? file;
            public bool? write;
            public bool? console;
            public bool? delete;
            public bool? noExt;
            public string ext;
            public bool valid;
            public string data;
            public string fileName;
        }

        static bool DONE = false;
        static char MARKER = 'h';
        static void Main ( string [] args ) {
            if ( args.Count ( ) < 1 ) {
                HelpText ( );
            } else {
                // Parse the argument given and check if the arguments are valid
                Config config = ParseArguments ( args );
                if ( !config.valid ) {
                    Environment.Exit ( 1 );
                }
                StringBuilder text = new StringBuilder ( );
                Console.WriteLine ( "Please type in the keys. Ctrl-C once done" );
                Console.CursorVisible = false; // Disable the cursor
                // We are using Ctrl-C as a signal to stop reading as enter key does not always signalify 
                // the end of a key
                Console.CancelKeyPress += new ConsoleCancelEventHandler (
                    ( object sender, ConsoleCancelEventArgs cArgs ) => cArgs.Cancel = DONE = true );
                while ( !DONE ) {
                    // Read the key press and store it
                    // For some safety, do not move the cursor when a key is pressed
                    text.Append ( Console.ReadKey ( true ).KeyChar );
                }
                string [] result = text.ToString ( ).Split ( MARKER );
                if ( result.Count ( ) != 2 || result [ 0 ].Count ( ) < 4 || result [ 1 ].Count ( ) < 4 ) {
                    // We want an obscure message about the keys to prevent anyone from figuring out anything
                    Console.WriteLine ( "Invalid Keys!" );
                    Environment.Exit ( 1 );
                }
                Console.CursorVisible = true;
                Encryption crypt = new Encryption ( result [ 0 ], result [ 1 ] );
                if ( !ProcessData ( crypt, config ) ) {
                    Environment.Exit ( 1 );
                }
                if ( !(config.delete ?? false) )
                    File.Delete ( config.data );
            }
        }

        static bool ProcessData ( Encryption crypt, Config config ) {
            string fileName = "";
            // If a filename was given, we are to assume to write to file
            if ( !String.IsNullOrWhiteSpace ( config.fileName ) ) {
                // If the file option is set, make sure that the input file exists
                if ( ( config.file ?? false ) && !File.Exists ( config.data ) ) {
                    Console.WriteLine ( "Input file does not exist!" );
                    return false;
                }
                // Get the filename to write to. Also check if we need to have an extension
                // Then attach the given extension if one was given
                if ( ( config.noExt ?? false ) ) {
                    fileName = Path.GetFileNameWithoutExtension ( config.fileName );
                } else { 
                    if (String.IsNullOrWhiteSpace(config.ext)) {
                        fileName += config.fileName + ( config.encrypt ? ".en" : ".de" );
                    } else {
                        fileName += config.fileName + config.ext;
                    }
                }
            }

            byte [] result;
            // If the data is NOT a file
            if ( !(config.file ?? false ) ) {
                // Encrypt/Decrypt it
                result = config.encrypt ? crypt.Encrypt ( config.data ) : crypt.Decrypt ( config.data );
                // If we were given a file, just write it out
                if (!String.IsNullOrEmpty(fileName)) {
                    using ( BinaryWriter stream = new BinaryWriter ( new FileStream ( fileName, FileMode.Create ) ) ) {
                        stream.Write ( result );
                    }
                } if ((config.console ?? true)) {
                    Console.Write ( "\nResult: 0x" );
                    result.ToList ( ).ForEach ( x => Console.Write ( "{0:x}", x ) );
                    Console.Write ( "\nResult in ASCII: " + Encoding.ASCII.GetString ( result ) );
                }
                return true;
            } else {
                using ( FileStream input = new FileStream ( config.data, FileMode.Open ), output = new FileStream ( fileName, FileMode.Create ) ) {
                    if ( config.encrypt ) crypt.Encrypt ( input, output );
                    else crypt.Decrypt ( input, output );
                }
                return true;
            }
        }

        static Config ParseArguments ( string [] args ) {
            Config config = new Config ( );

            // Enforce the first argument to be either encrypt or decrypt
            if ( args [ 0 ].Equals ( "encrypt" ) )
                config.encrypt = true;
            else if ( args [ 0 ].Equals ( "decrypt" ) )
                config.encrypt = false;
            else {
                Console.WriteLine ( "Please specify whether the data given is to be encrypted or decrypted first" );
                config.valid = false;
                return config;
            }

            // Evaluate the arguments
            for ( int i = 1; i < args.Count ( ); i++ ) {
                if ( args [ i ] [ 0 ] == '-' ) {
                    switch ( args [ i ] ) {
                        case "-w":
                        case "--write":
                            config.write = true;
                            config.fileName = args [ ++i ];
                            break;
                        case "-f":
                        case "--file":
                            config.write = true;
                            config.file = true;
                            break;
                        case "--no-display":
                            config.console = false;
                            break;
                        case "--delete-file":
                            config.delete = true;
                            break;
                        case "--no-extension":
                            config.noExt = true;
                            config.ext = "";
                            break;
                        case "--extension":
                            config.noExt = false;
                            config.ext = args [ ++i ];
                            break;
                        default:
                            break;
                    }
                } else {
                    config.data = args [ i ];
                    // If we are writing to a file, but the filename was never given, assume that the
                    // input file is the output filename
                    if ( ( config.file ?? false ) && String.IsNullOrWhiteSpace ( config.fileName ) ) {
                        config.fileName = config.data;
                    }
                    break;
                }
            }

            config.valid = false;
            if ( string.IsNullOrEmpty ( config.data ) ) {
                return config;
            }

            // check consistency
            if ( ( !String.IsNullOrWhiteSpace ( config.ext ) || ( config.noExt ?? false ) ) &&
            !( ( config.write ?? false ) || ( config.file ?? false ) ) ) {
                Console.WriteLine ( "Extension options only work when either -w or -f is enabled" );
            } else if ( ( config.delete ?? false ) && !( config.file ?? false ) ) {
                Console.WriteLine ( "--delete-file only works when -f is enabled" );
            } else {
                config.valid = true;
            }
            return config;
        }

        static void HelpText ( ) => Console.WriteLine ( "Usage: crypt {encrypt|decrypt} {option} data\n" +
            "Options allowed are:\n" +
            "-f | --file\t\tDenote that the data is a file. Implies -w --force-binary\n" +
            "-w {file} | --write {file}\t\tWrite the resulting encrypted/decrypted data to file. Unless an extension is given, the default\n" +
            "\t\t\t extension is .en for encryption and .de for decryption\n" +
            "--no-display\t\tDo NOT display the resulting encrypted/decrypted data in console\n" +
            "--delete-file\t\tDelete the file given after encryption/decrypt. Enable by default with -f option\n" +
            "--no-extension\t\tOutput the resulting encrypted/decrypted file without any extension. Only works with -f or -w option.\n " +
            "\t\t\tDoes not work with --extension\n" +
            "--extension {ext}\tOutput the resulting encrypted/decrypted file with the proved extension. Only works with -f or -w option.\n" +
            "\t\t\tDoes not work with --no-extensions\n" +
            "-h | --help\t\tDisplay this help message\n" +
            "\n" +
            "After the options and data are given, you will be prompted to enter the keys. The keys must be of length 4\n" +
            "Or longer. The order of the keys matters.\n\n" +
            "by default, the resulting data will be display on console unless -w or -f is specified. If -f is given, by default, the data will" +
            "be written to a file with the same name but with the the .en/.de extension."
            );
    }
}
